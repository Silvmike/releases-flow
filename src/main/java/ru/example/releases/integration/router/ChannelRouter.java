package ru.example.releases.integration.router;

import org.springframework.integration.router.RecipientListRouter;
import org.springframework.messaging.MessageChannel;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ChannelRouter extends RecipientListRouter {

    public int removeRecipient(MessageChannel messageChannel) {

        Collection<Recipient> oldRecipientList = getRecipients();
        List<Recipient> newRecipientList = oldRecipientList.stream().filter(r -> r.getChannel() != messageChannel).collect(Collectors.toList());
        setRecipients(newRecipientList);
        return oldRecipientList.size() - newRecipientList.size();
    }
}
