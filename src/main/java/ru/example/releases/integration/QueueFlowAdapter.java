package ru.example.releases.integration;

import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.dsl.IntegrationFlowAdapter;
import org.springframework.integration.dsl.IntegrationFlowDefinition;
import org.springframework.integration.dsl.Pollers;
import org.springframework.messaging.MessageHandler;
import ru.example.releases.integration.router.ChannelRouter;

public class QueueFlowAdapter extends IntegrationFlowAdapter {

    private MessageHandler messageHandler;
    private ChannelRouter router;
    private QueueChannel channel;

    public QueueFlowAdapter(ChannelRouter router, QueueChannel channel) {
        this(new Reporter(), router, channel);
    }

    public QueueFlowAdapter(MessageHandler messageHandler, ChannelRouter router, QueueChannel channel) {
        this.messageHandler = messageHandler;
        this.router = router;
        this.channel = channel;
    }

    @Override
    protected IntegrationFlowDefinition<?> buildFlow() {
        router.addRecipient(channel, (m) -> channel.getFullChannelName().equals(m.getHeaders().get(Headers.QUEUE)));
        return from(channel).handle(messageHandler, spec -> spec.poller(Pollers.fixedDelay(200L)));
    }

}
