package ru.example.releases.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlowAdapter;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import ru.example.releases.integration.router.ChannelRouter;

@Configuration
public class HeadFlow {

    @Bean
    public IntegrationFlow headFlow(
            @Autowired @Qualifier(Channels.CHANGES) SubscribableChannel inputChannel,
            @Autowired @Qualifier(Channels.ROUTER) MessageChannel routerChannel,
            @Autowired @Qualifier(Channels.HEAD) QueueChannel headQueue
    ) {

        return IntegrationFlows.from(inputChannel)
                               .enrichHeaders(spec -> spec.header(Headers.QUEUE, headQueue.getFullChannelName()))
                               .channel(routerChannel)
                               .get();
    }

    @Bean
    public IntegrationFlowAdapter headQueueFlow(
            ChannelRouter router,
            @Autowired @Qualifier(Channels.HEAD) QueueChannel headQueue
    ) {
        return new QueueFlowAdapter(router, headQueue);
    }

    @Bean(name = Channels.HEAD)
    public QueueChannel headQueue() {
        return new QueueChannel();
    }

}
