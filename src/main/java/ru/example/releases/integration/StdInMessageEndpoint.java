package ru.example.releases.integration;

import org.springframework.integration.endpoint.MessageProducerSupport;
import org.springframework.integration.support.MessageBuilder;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class StdInMessageEndpoint extends MessageProducerSupport {

    private Thread thread;

    @Override
    protected void doStart() {
        this.thread = new Thread(() -> {

            Scanner scanner = new Scanner(System.in);
            while (!Thread.currentThread().isInterrupted()) {

                try {
                    String line = scanner.nextLine();
                    sendMessage(MessageBuilder.withPayload(line).build());
                } catch (NoSuchElementException e) {
                    break;
                }
            }
        });
        //this.thread.setDaemon(true);
        this.thread.setName("stdin-thread");
        this.thread.start();
    }

    @Override
    protected void doStop() {
        this.thread.interrupt();
        this.thread = null;
    }
}
