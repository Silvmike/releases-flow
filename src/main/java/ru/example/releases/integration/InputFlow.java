package ru.example.releases.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;
import ru.example.releases.domain.ArtifactTransformer;

import java.util.concurrent.Executor;

@Configuration
public class InputFlow {

    public static final String CHANGES_EXECUTOR = "changesExecutor";

    @Bean
    public IntegrationFlow inputFlow(@Autowired @Qualifier(Channels.CHANGES) SubscribableChannel input) {
        return IntegrationFlows.from(new StdInMessageEndpoint()).transform(new ArtifactTransformer()).channel(input).get();
    }

    @Bean(name = Channels.CHANGES)
    public PublishSubscribeChannel changesChannel(@Autowired @Qualifier(CHANGES_EXECUTOR) Executor executor) {
        return new PublishSubscribeChannel(executor);
    }

    @Bean(name = CHANGES_EXECUTOR)
    public ThreadPoolExecutorFactoryBean changesExecutor() {
        ThreadPoolExecutorFactoryBean executorFactory = new ThreadPoolExecutorFactoryBean();
        executorFactory.setCorePoolSize(1);
        executorFactory.setMaxPoolSize(1);
        executorFactory.setAwaitTerminationSeconds(10);
        executorFactory.setWaitForTasksToCompleteOnShutdown(true);
        return executorFactory;
    }

}

