package ru.example.releases.integration;

public interface Channels {

    String CHANGES = "changesChannel";
    String ROUTER = "routerChannel";
    String HEAD = "headQueueChannel";
    String QUEUE_1 = "queue1queueChannel";

}
