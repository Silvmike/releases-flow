package ru.example.releases.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.dsl.IntegrationFlowAdapter;
import ru.example.releases.integration.router.ChannelRouter;

@Configuration
public class QueueFlow {

    @Bean
    public IntegrationFlowAdapter queue1QueueFlow(
            ChannelRouter router,
            @Autowired @Qualifier(Channels.QUEUE_1) QueueChannel headQueue
    ) {
        return new QueueFlowAdapter(router, headQueue);
    }

    @Bean(name = Channels.QUEUE_1)
    public QueueChannel queue1Queue() {
        return new QueueChannel();
    }

}
