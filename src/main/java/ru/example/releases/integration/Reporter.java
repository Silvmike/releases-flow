package ru.example.releases.integration;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

public class Reporter implements MessageHandler {

    @Override
    public void handleMessage(Message<?> message) throws MessagingException {
        System.out.println(String.format("[%s] %s: %s", Thread.currentThread().getName(), message.getHeaders().get(Headers.QUEUE), message.getPayload().toString()));
    }
}
