package ru.example.releases.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.messaging.MessageChannel;
import ru.example.releases.integration.router.ChannelRouter;

@EnableIntegration
@Configuration
public class ReleaseFlow {

    @Bean
    public IntegrationFlow releaseFlow(
            @Autowired @Qualifier(Channels.ROUTER) MessageChannel inputChannel,
            ChannelRouter router
    ) {

        return IntegrationFlows.from(inputChannel)
                               .route(router).get();
    }

    @Bean
    public ChannelRouter router() {
        return new ChannelRouter();
    }

    @Bean(Channels.ROUTER)
    public MessageChannel routerChannel() {
        return MessageChannels.direct().get();
    }

}
