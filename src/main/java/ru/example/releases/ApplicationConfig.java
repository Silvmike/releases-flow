package ru.example.releases;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.*;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ru.example.releases.controller.ManualController;
import ru.example.releases.integration.HeadFlow;
import ru.example.releases.integration.InputFlow;
import ru.example.releases.integration.QueueFlow;
import ru.example.releases.integration.ReleaseFlow;

@Import({
    InputFlow.class,
    ReleaseFlow.class,
    QueueFlow.class,
    HeadFlow.class
})
@Configuration
@EnableWebMvc
@EnableIntegration
@ComponentScan(
    useDefaultFilters = false,
    basePackageClasses = ManualController.class,
    includeFilters = @ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE,
        classes = ManualController.class
    )
)
@SpringBootApplication
public class ApplicationConfig {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationConfig.class, args);
    }

}