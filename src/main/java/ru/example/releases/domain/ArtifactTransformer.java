package ru.example.releases.domain;

import org.springframework.integration.transformer.AbstractTransformer;
import org.springframework.messaging.Message;

public class ArtifactTransformer extends AbstractTransformer {

    @Override
    protected Object doTransform(Message<?> message) {
        return Artifact.fromString((String) message.getPayload());
    }

}
