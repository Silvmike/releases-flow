package ru.example.releases.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.example.releases.integration.Channels;
import ru.example.releases.integration.Headers;
import ru.example.releases.domain.Artifact;

@RestController
public class ManualController {

    @Autowired @Qualifier(Channels.ROUTER) private MessageChannel routerChannel;
    @Autowired @Qualifier(Channels.QUEUE_1) private QueueChannel queueQueue;

    @RequestMapping(method = RequestMethod.POST, path = "/version")
    public void postVersion(@RequestBody String version) {
        routerChannel.send(
            MessageBuilder.withPayload(
                    Artifact.fromString(version)
            ).setHeader(Headers.QUEUE, queueQueue.getFullChannelName())
            .build()
        );
    }

}
